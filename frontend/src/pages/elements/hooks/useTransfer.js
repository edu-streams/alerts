import { useRef, useEffect } from "react";
import transferUsers from "../functions/transferUsers";

const useTransfer = ({ tempArray, setAnimationArray, resetTempArray }) => {
  const timeoutRef = useRef();

  useEffect(() => {
    if (tempArray.length === 0) return;

    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }

    timeoutRef.current = setTimeout(() => {
      transferUsers({ tempArray, setAnimationArray, resetTempArray });
    }, 2000);

    return () => {
      clearTimeout(timeoutRef.current);
    };
  }, [tempArray]);
};

export default useTransfer;
