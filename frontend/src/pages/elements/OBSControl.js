import {
  ToggleButton,
  ToggleButtonGroup,
  TextField,
  Box,
  Alert,
} from "@mui/material";
import { useEffect, useState } from "react";
import Typography from "@mui/material/Typography";
import { socket } from "../../services/socket";

const OBSControl = () => {
  const [isDisplay, setIsDisplay] = useState(false);
  const [firstAnimationThreshold, setFirstAnimationThreshold] = useState(15);
  const [secondAnimationThreshold, setSecondAnimationThreshold] = useState(30);

  const firstFieldText = `First user animation timeout:`;
  const secondFieldText = `Second user animation timeout:`;

  const handleChange = (event) => {
    if (event.target.value === "false") {
      setIsDisplay(false);
    }
    if (event.target.value === "true") {
      setIsDisplay(true);
    }
  };

  const setFirstValue = (event) => {
    if (event.target.value === "") return;
    if (event.target.value < 1 || event.target.value > 20) return;
    setFirstAnimationThreshold(Number.parseInt(event.target.value));
  };

  const setSecondValue = (event) => {
    if (event.target.value === "") return;
    if (event.target.value < 1 || event.target.value > 50) return;
    setSecondAnimationThreshold(Number.parseInt(event.target.value));
  };

  useEffect(() => {
    const alertOptions = {
      isDisplay,
      firstAnimationThreshold,
      secondAnimationThreshold,
    };

    if (isDisplay) console.log("Sending data to layout:", alertOptions);

    socket.emit("control", alertOptions);
  }, [isDisplay]);

  return (
    <>
      <Typography>OBS Control</Typography>

      <Box sx={{ display: "flex", justifyContent: "center", height: "100px" }}>
        {secondAnimationThreshold !== firstAnimationThreshold &&
        secondAnimationThreshold > firstAnimationThreshold ? (
          <ToggleButtonGroup
            color="primary"
            value={isDisplay}
            exclusive
            onChange={handleChange}
            aria-label="Platform"
          >
            <ToggleButton disabled={isDisplay} value={true}>
              Enable
            </ToggleButton>
            <ToggleButton disabled={!isDisplay} value={false}>
              Disable
            </ToggleButton>
          </ToggleButtonGroup>
        ) : (
          <Alert variant="filled" severity="error">
            Values must be different and second field value must be higher than
            first one!
          </Alert>
        )}
      </Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          marginTop: "10px",
          padding: "10px",
          border: "1px solid rgba(0, 0, 0, 0.5)",
          borderRadius: "10px",
        }}
      >
        <TextField
          sx={{ marginRight: "10px" }}
          id="outlined-controlled"
          type="number"
          disabled={isDisplay}
          label={firstFieldText}
          value={firstAnimationThreshold}
          onChange={(event) => setFirstValue(event)}
        />
        <TextField
          id="outlined-controlled"
          type="number"
          disabled={isDisplay}
          label={secondFieldText}
          value={secondAnimationThreshold}
          onChange={(event) => setSecondValue(event)}
        />
      </Box>
    </>
  );
};
export default OBSControl;
