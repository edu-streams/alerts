import React from "react";

import sunWithAvatar from "../../../images/SunAvaOpt.gif";
import tempAvatar from "../../../images/AvatarExample.png";

// SAT => second animation threshold

const SATList = ({ userToDisplayOnSAT }) => {
  return (
    <>
      {userToDisplayOnSAT.map(({ id, avatar }) => {
        const randomStartFromTop = Math.ceil(Math.random() * (500 - 75) + 75);
        const randomStartFromLeft = Math.ceil(Math.random() * (100 - 50) + 50);
        return (
          <li
            key={id}
            className="appearItem"
            style={{
              top: `${randomStartFromTop}px`,
              left: `${randomStartFromLeft}%`,
            }}
          >
            <img
              src={sunWithAvatar}
              alt="Sun shine with avatar"
              width="200px"
            />
            <img
              className="userIsComing"
              src={tempAvatar}
              alt="Funny pilot shows thumb up"
              width="70px"
            />
          </li>
        );
      })}
    </>
  );
};

const SATListMemo = React.memo(SATList);

export default SATListMemo;
