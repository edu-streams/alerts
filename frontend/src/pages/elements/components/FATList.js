import React from "react";

import cloudWithName from "../../../images/CloudSmileOpt.gif";

// FAT => first animation threshold

const FATList = ({ userToDisplayOnFAT }) => {
  return (
    <>
      {userToDisplayOnFAT.map(({ id, name }) => {
        const randomStartFromTop = Math.ceil(Math.random() * (500 - 25) + 25);
        const randomStartFromLeft = Math.ceil(Math.random() * (100 - 50) + 50);
        return (
          <li
            key={id}
            className="appearItem"
            style={{
              top: `${randomStartFromTop}px`,
              left: `${randomStartFromLeft}%`,
            }}
          >
            <img
              src={cloudWithName}
              alt="Smiling cloud with user name"
              width="200px"
            />
            <p className="userIsComing">{name}</p>
          </li>
        );
      })}
    </>
  );
};

const FATListMemo = React.memo(FATList);

export default FATListMemo;
