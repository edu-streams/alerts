import React from "react";

function DisconnectedUser() {
  return <div>DisconnectedUser</div>;
}

const DisconnectedUserMemo = React.memo(DisconnectedUser);

export default DisconnectedUserMemo;