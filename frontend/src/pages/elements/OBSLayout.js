import React, { useEffect, useState, useMemo, useRef } from "react";
import { socket } from "../../services/socket";
import Typography from "@mui/material/Typography";
import { Box, TextField } from "@mui/material";

// FAT - first animation threshold
// SAT - second animation threshold

import useTransfer from "./hooks/useTransfer";

import FATListMemo from "./components/FATList";
import SATListMemo from "./components/SATList";

import cloudWithThunder from "../../images/CloudExitOpt.gif";

import "../../ui/layout.css";

const OBSLayout = () => {
  const [animationThresholds, setAnimationThresholds] = useState({
    firstThreshold: 15,
    secondThreshold: 30,
  });

  const [isActive, setIsActive] = useState(true);

  const [listOfUsers, setListOfUsers] = useState([]);

  const [userToDisplayOnFAT, setUserToDisplayOnFAT] = useState([]);
  const [userToDisplayOnSAT, setUserToDisplayOnSAT] = useState([]);
  const [offlineUsers, setOfflineUsers] = useState([]);

  const [tempFAT, setTempFAT] = useState([]);
  const [tempSAT, setTempSAT] = useState([]);

  const timestampRef = useRef(new Date().getTime());

  useEffect(() => {
    const onControlChange = ({
      isDisplay,
      firstAnimationThreshold,
      secondAnimationThreshold,
    }) => {
      const firstThreshold = Number(firstAnimationThreshold);
      const secondThreshold = Number(secondAnimationThreshold);

      console.log("FAT:", firstThreshold);
      console.log("SAT:", secondThreshold);

      setAnimationThresholds({
        firstThreshold,
        secondThreshold,
      });

      if (!isDisplay) console.log("IsDisplay after turning off:", isDisplay);

      setIsActive(isDisplay);
    };

    socket.on("control", onControlChange);

    return () => {
      socket.off("control");
    };
  }, []);

  useEffect(() => {
    const onUserConnect = (userData) => {
      console.log("New user is coming:", userData);
      const isConnected = listOfUsers.some((user) => user.id === userData.id);

      if (!isConnected) {
        setListOfUsers((prevState) => [
          ...prevState,
          {
            ...userData,
            timestamp: new Date().getTime(),
          },
        ]);
        // return;
      }

      // setListOfUsers((listOfUsers) =>
      //   listOfUsers.map((_user) => {
      //     if (userData.id === _user.id) {
      //       return {
      //         ..._user,
      //         // status: "ONLINE" || "OFFLINE",
      //         status: _user.status,
      //         timestamp: new Date().getTime(),
      //       };
      //     }
      //     return {
      //       ..._user,
      //       status: _user.status,
      //       timestamp: new Date().getTime(),
      //     };
      //   })
      // );
      setUsers((users) =>
        users.map((_user) => {
          if (userData.id == _user.id) {
            return {
              ..._user,
              status: _user.status,
              timestamp: new Date().getTime(),
            };
          }
          return _user;
        })
      );
    };

    socket.on("userIsConnected", onUserConnect);
    // socket.on("userIsDisconnected", onUserConnect);

    return () => {
      socket.off("userIsConnected");
      // socket.off("userIsDisconnected");
    };
  }, []);

  useEffect(() => {
    if (listOfUsers.length === 0) return;

    const { firstThreshold, secondThreshold } = animationThresholds;

    const restOfFirstThreshold = listOfUsers.length % firstThreshold;

    const restOfSecondThreshold = listOfUsers.length % secondThreshold;

    const userToAdd = listOfUsers[listOfUsers.length - 1];

    if (restOfFirstThreshold === 0 && restOfSecondThreshold === 0) {
      if (tempSAT.length >= 5) return;
      setTempSAT((prevState) => [...prevState, { ...userToAdd }]);
    } else if (restOfSecondThreshold === 0) {
      if (tempSAT.length >= 5) return;
      setTempSAT((prevState) => [...prevState, { ...userToAdd }]);
    } else if (restOfFirstThreshold === 0) {
      console.log("Inside first check");
      if (tempFAT.length >= 5) return;
      setTempFAT((prevState) => [...prevState, { ...userToAdd }]);
    }

    timestampRef.current = new Date().getTime();
  }, [listOfUsers]);

  // useEffect(() => {
  //   const offlineUsersArray = listOfUsers.filter(
  //     (user) => user.status === "OFFLINE"
  //   );
  //   setOfflineUsers(offlineUsersArray);
  //   console.log("OfflineUsersArray:", offlineUsersArray);
  //   console.log("Result array:", offlineUsers);
  // }, [listOfUsers]);

  useTransfer({
    tempArray: tempFAT,
    setAnimationArray: setUserToDisplayOnFAT,
    resetTempArray: setTempFAT,
  });

  useTransfer({
    tempArray: tempSAT,
    setAnimationArray: setUserToDisplayOnSAT,
    resetTempArray: setTempSAT,
  });

  return (
    <>
      <Typography>OBS Layout</Typography>
      <div className="alertsWindow">
        <ul className="animationsList">
          <FATListMemo userToDisplayOnFAT={userToDisplayOnFAT} />
          <SATListMemo userToDisplayOnSAT={userToDisplayOnSAT} />
          {/* {offlineUsers.map(({ id, name }) => {
            <li key={id} className="disapperItem">
              <span className="offlineUserName">{name}</span>
              <img
                src={cloudWithThunder}
                alt="Smiling cloud with user name"
                width="200px"
              />
            </li>;
          })} */}
        </ul>
        {/* {userToDisplayOnSAT.length !== 0 &&
          userToDisplayOnSAT.map(({ id, avatar }) => (
            <div key={id} className="appearContainer appearContainer--lower">
              <img
                src={sunWithAvatar}
                alt="Sun shine with avatar"
                width="200px"
              />
              <img
                className="userAvatarOnSAT"
                src={avatarExample}
                alt="Funny pilot shows thumb up"
                width="70px"
              />
            </div>
          ))} */}
        {/* {userIsLeaving && (
          <div className="disappearContainer">
            <p className="userName">
              {listOfUsers[listOfUsers.length - 1]?.name}
            </p>
          </div>
        )} */}
      </div>
    </>
  );
};
export default OBSLayout;
