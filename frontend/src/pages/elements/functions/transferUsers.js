const transferUsers = ({ tempArray, setAnimationArray, resetTempArray }) => {
  setAnimationArray([...tempArray]);
  setTimeout(() => {
    resetTempArray([]);
  }, 4000);
};

export default transferUsers;
