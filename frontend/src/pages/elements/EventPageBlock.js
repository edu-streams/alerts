import Typography from "@mui/material/Typography";
import { Box, Button } from "@mui/material";
import { useState } from "react";
import { socket } from "../../services/socket";

const EventPageBlock = () => {
  const [idIsComing, setIdIsComing] = useState(1);
  const [idIsLeaving, setIdIsLeaving] = useState(1);

  const connectFakeUser = () => {
    const user = {
      id: idIsComing,
      name: `UserToCome ${idIsComing}`,
      status: "ONLINE",
      avatar: "AvatarURL",
    };
    console.log("Sending new user:", user);
    setIdIsComing(idIsComing + 1);

    socket.emit("userIsConnected", user);
  };

  const disconnectFakeUser = () => {
    const user = {
      id: idIsLeaving,
      name: `UserToLeave ${idIsLeaving}`,
      status: "OFFLINE",
      avatar: "AvatarURL",
    };
    setIdIsLeaving(idIsLeaving + 1);
    socket.emit("userIsDisconnected", user);
  };

  return (
    <Box>
      <Typography>Event page block</Typography>
      <Button onClick={() => connectFakeUser()}>
        `Connect user with id {idIsComing}`
      </Button>
      <Button onClick={() => disconnectFakeUser()}>
        `Disconnect user with id {idIsLeaving}`
      </Button>
    </Box>
  );
};
export default EventPageBlock;
